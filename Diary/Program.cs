﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"data.csv"; // путь к файлу

            Repository rep = new Repository(path);

            int option = 0; // опции управления программой

            while (option != 3)
            {
                Console.WriteLine("Введите <1> чтобы добавить запись; <2> чтобы посмотреть список задач; <3> чтобы сохранить данные и выйти");

                bool result = int.TryParse(Console.ReadLine(), out option);

                //Console.WriteLine($"result = {result}, option = {option}");

                switch (option)
                {
                    case 1:
                        Console.WriteLine("Введите дату и время");

                        //Console.ReadLine();
                        //DateTime date1 = new DateTime();

                        string[] separators = { ",", ":", "." };
                        string[] date = Console.ReadLine().Split(separators, StringSplitOptions.RemoveEmptyEntries);
                        
                        DateTime date1 = new DateTime(long.Parse(date));
                        rep.Add(new Note(date1.ToString("d"), "20:00", "поесть", "важно", "активно"));
                        break;
                    case 2:
                        Console.Clear();
                        rep.PrintDbToConsole();
                        break;
                    case 3:
                        rep.Save("newdata.csv");
                        File.Copy("newdata.csv", path, true);
                        break;
                    default:
                        continue;
                }
            }              

            //rep.PrintDbToConsole(); // вывод данных в консоль
            //rep.Add(new Note("09.03.2020", "20:00", "поесть", "важно", "активно")); // добовление новой записи
            //rep.Save("data.csv"); // сохранение данных
        }
    }
}
