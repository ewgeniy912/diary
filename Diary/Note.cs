﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    public struct Note
    {
        /// <summary>
        /// Создание записи
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Time"></param>
        /// <param name="Events"></param>
        /// <param name="Priority"></param>
        /// <param name="Status"></param>
        public Note(string Date, string Time, string Events, string Priority, string Status)
        {
            this.date = Date;
            this.time = Time;
            this.events = Events;
            this.priority = Priority;
            this.status = Status;
        }

        #region Свойства
        /// <summary>
        /// Дата (dd.ММ.YYYY)
        /// </summary>
        public string Date { get { return this.date; } set { this.date = value; } }

        /// <summary>
        /// Время
        /// </summary>
        public string Time { get { return this.time; } set { this.time = value; } }

        /// <summary>
        /// Описание события / задачи
        /// </summary>
        public string Events { get { return this.events; } set { this.events = value; } }

        /// <summary>
        /// Приоритет события / задачи (высокий / средний / низкий)
        /// </summary>
        public string Priority { get { return this.priority; } set { this.priority = value; } }

        /// <summary>
        /// Статус события / задачи (          / выполнено / перенесено / отменено)
        /// </summary>
        public string Status 
        { 
            get { return this.status; }
            set 
            { 
                if (status != "выполнено")
                {
                    this.status = value;
                }                 
            } 
        }
        #endregion

        #region поля
        /// <summary>
        /// Дата (dd.ММ.YYYY)
        /// </summary>
        private string date;

        /// <summary>
        /// Время
        /// </summary>
        private string time;

        /// <summary>
        /// Описание события / задачи
        /// </summary>
        private string events;

        /// <summary>
        /// Приоритет события / задачи (высокий / средний / низкий)
        /// </summary>
        private string priority;

        /// <summary>
        /// Статус события / задачи (          / выполнено / перенесено / отменено)
        /// </summary>
        private string status;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Print()
        {
            return $"{this.date,10}|{DateTime.Parse(Date).ToString("ddd"),3} | {this.time} |{this.events,32}|{this.priority,9}|{this.status}";
        }
    }
}
