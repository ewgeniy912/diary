﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    struct Repository
    {
        private List<Note> notes;

        private string path;

        /// <summary>
        /// Констрктор
        /// </summary>
        /// <param name="Path">Путь к файлу с данными</param>
        public Repository(string Path)
        {
            this.path = Path; // Сохранение пути к файлу с данными
            this.notes = new List<Note>(); // инициализаия массива заметок.

            this.Load(); // Загрузка данных
        }

        /// <summary>
        /// Метод добавления записей в хранилище
        /// </summary>
        /// <param name="NewNote">новая запись</param>
        public void Add(Note NewNote)
        {
            this.notes.Add(NewNote);
        }

        /// <summary>
        /// Метод загрузки данных
        /// </summary>
        private void Load()
        {
            using (StreamReader sr = new StreamReader(this.path))
            {
                while (!sr.EndOfStream)
                {
                    string[] args = sr.ReadLine().Split(',');

                    Add(new Note(args[0], args[1], args[2], args[3], args[4]));
                }
            }
        }

        /// <summary>
        /// Метод сохранения данных
        /// </summary>
        /// <param name="Path"></param>
        public void Save(string Path)
        {

            for (int i = 0; i < this.notes.Count; i++)
            {
                string temp = String.Format("{0},{1},{2},{3},{4}",
                                        this.notes[i].Date,
                                        this.notes[i].Time,
                                        this.notes[i].Events,
                                        this.notes[i].Priority,
                                        this.notes[i].Status);

                File.AppendAllText(Path, $"{temp}\n");
            }
        }

        /// <summary>
        /// Метод вывода данных в консоль
        /// </summary>
        public void PrintDbToConsole()
        {
            Console.WriteLine($"{"Число",7}   | Д/н|{"время",6} |         Задача/Событие         |приоритет| статус  |\n" +
                              $"----------+----+-------+--------------------------------+---------+---------+");

            foreach (var note in this.notes)
            {
                Console.WriteLine(note.Print());
            }
        }

        public void SortNote()
        {
            var tnotes = (from n in notes
                    orderby n.Time ascending
                    select n);
        }
    }
}